import React, { useReducer} from 'react'
import { reducer } from '../reducer/reducer'
import { initialstate } from '../reducer/reducer'

export const Counter = () => {

    

    const [ state, dispatch ] = useReducer(reducer, initialstate)
  return (
    <div>

        Count: { state.count}

        <button onClick={() => dispatch({ type: 'increment', payload: 10})}>Increment</button>
        <button onClick={() => dispatch({ type: 'decrement', payload: 10})}>Decrement</button>
        <button onClick={() => dispatch({ type: 'reset'})}>Reset</button>


    </div>
  )
}
