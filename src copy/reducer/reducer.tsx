
export const initialstate = { count: 0}

type CounterState = {
    count: number
}


type UpdateAction = {
    type: 'increment' | 'decrement'
    payload: number
}

type ResetAction = {
    
    type: 'reset'
    
    
}
type CounterAction = ResetAction | UpdateAction


export const reducer = (state: CounterState, action: CounterAction) => {
    switch(action.type){


        case 'increment':
        return { count: state.count + action.payload}
        case 'decrement':
            return { count: state.count - action.payload}
        case 'reset':
            return initialstate

        default: 
        return state
    }
}